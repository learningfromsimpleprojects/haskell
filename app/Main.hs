module Main where
import System.Environment

-- Here is the program:
myFun = putStrLn "I just got executed"

main :: IO ()
main = do
	myFun
	myFun
	myFun

