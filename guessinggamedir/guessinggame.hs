module Main where
import System.Environment
import System.Random (randomRIO)
main :: IO ()
main = do
    unknown <- randomRIO (1, 999999)
    guessingGame unknown 1 where
    guessingGame :: Integer -> Integer -> IO()
    guessingGame guessNum counter = do
        putStrLn "haskell is death. I mean guess a number:"
        guessed <- readLn
        case compare guessNum guessed of
            LT -> do
                putStrLn "Too high, try again."
                guessingGame guessNum (counter + 1)
            GT -> do
                putStrLn "Too low, try again."
                guessingGame guessNum (counter + 1)
            EQ -> do
                putStrLn("You won in " ++ show(counter) ++ " tries!")